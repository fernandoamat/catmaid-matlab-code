%first execute pairwiseSegmentationLinakgeError.m to get matrix info

%for each element of ground truth it goes backwards in time deltaT time
%points and checks if it is error free

%trackingMatrixGT fields
%id, type, x, y, z, radius, parent_id, time, confidence, skeletong_id
function [lineagesNerrFree, lineagesNerrFreeGT] = errorFreeLineageBackwardsT(projectName, trackingMatrixGT, trackingMatrixErrorCode)


%annotations can have gaps without confidence = 5 (I need whole skeleton)
%main parameters
if(nargin < 1)
    projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast'; %project containing the ground truth in CATMAID
end

pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

tic;
%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( strcmp(projectName,'12-08-28 Drosophila TGMM TM1000 Neuroblast') == 1 )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points for each skeleton
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
trackingMatrix = zeros(size(trackingMatrixGT,1)*10, size(trackingMatrixGT,2) );
N = 0;

skeletonIdVec = unique( trackingMatrixGT(:,10) );
for ii = 1:length(skeletonIdVec)
    skeletonId = skeletonIdVec(ii);
    WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND skeleton_id = ' num2str(skeletonId)];
    trackingMatrixAux = retrieveLineageFilter( connDB, WHEREclause );
    
    for kk = 1: 3
        trackingMatrixAux(:,kk+2) = scale(kk) * trackingMatrixAux(:,kk + 2) / stackRes(kk);
    end
    Naux = size(trackingMatrixAux,1);
    
    trackingMatrix(N+1:N+Naux,:) = trackingMatrixAux;
    N = N + Naux;
end
trackingMatrix = trackingMatrix(1:N,:);

treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

rmpath(pathAdd);
toc;

NGT = size( trackingMatrixGT,1);
treenodeIdMapGT = containers.Map(trackingMatrixGT(:,1), [1:size(trackingMatrixGT,1)]);

%-------------------------------------------------------
%remove branches that are not curated for the most part
tic;
isDivision = zeros(N,1);
for kk = 1:N
    nodeIdPar = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, nodeIdPar ) == true )%we do not have parent in ground truth
        aux = treenodeIdMap( nodeIdPar );
        isDivision( aux ) = isDivision( aux ) + 1;
    end
end
isLeaf = (isDivision == 0 & trackingMatrix(:,7) >= 0);
isDivision = (isDivision > 1);

posL = find( isLeaf == true | isDivision == true);

erase = zeros(N,1);
eraseN = 0;
for kk = 1: length(posL)
    eraseAux = zeros(1000,1);
    eraseAuxN = 0;
    numErr = 0;
    numGT = 0;
    nodeId = posL(kk);
    parId = trackingMatrix( nodeId,7);
    endTM = trackingMatrix( nodeId,8);
    iniTM = endTM; %so length of branch is endTM - iniTM + 1
    while( isKey( treenodeIdMap, parId ) == true )
        eraseAuxN = eraseAuxN + 1;
        eraseAux(eraseAuxN) = nodeId;
        %check if this node was in the gt subset
        if( isKey( treenodeIdMapGT, trackingMatrix(nodeId,1) ) == true )
            numGT = numGT + 1;
            nodeIdGT = treenodeIdMapGT( trackingMatrix(nodeId,1) );
            if( isKey( treenodeIdMapGT, trackingMatrixGT(nodeIdGT,7) ) == true && trackingMatrixErrorCode(nodeIdGT) < 2 )%parent is in the ground truth and segmentation or tracking failed
                numErr = numErr + 1;
            end
        end
        
        %update for next step
        par = treenodeIdMap( parId );
        if( isDivision( par ) == true )
            break;
        end
        nodeId = par;
        iniTM = trackingMatrix( par,8);
        parId = trackingMatrix( par,7);
        if( numGT == 0 )%do not start counting until the first curated poitn is hit
            endTM = iniTM;
        end
    end
    
    %only keept branches where the majority of cells have been curated
    branchL = endTM - iniTM + 1;
    if( numGT / branchL < 0.8 )
        erase(eraseN+1:eraseN+eraseAuxN) = eraseAux(1:eraseAuxN);
        eraseN = eraseN + eraseAuxN;
    end
end

erase = erase(1:eraseN);
trackingMatrix(erase,:) = [];
%redo mapping
N = size(trackingMatrix,1);
treenodeIdMap = containers.Map(trackingMatrix(:,1), [1:size(trackingMatrix,1)]);

toc;


%-------------------------------------------------------
%map children in order to traverse forward
childrenIdMap = -ones( size(trackingMatrix,1) , 2);%  treenodeIdMap ( trackingMatrix( childrenIdMap(i,1), 7 ) ) = i
for kk = 1:size(trackingMatrix,1)
    parId = trackingMatrix(kk,7);
    if( isKey( treenodeIdMap, parId ) == true )
        parIdx = treenodeIdMap( parId );
        if( childrenIdMap(parIdx,1) < 0 )
            childrenIdMap(parIdx,1) = kk;
        else
            childrenIdMap(parIdx,2) = kk;
        end
    end
end

%--------------------------------------------------------
%we know that branches that are left have been mostly curated ( we mainly
%removed spurious branches). Now we can do statistics
tic;

TMend = max( trackingMatrix(:,8) );
lineagesNerrFree = cell(TMend+1,1);
lineagesNerrFreeGT = lineagesNerrFree;%debuggign purposes

for frame = 0:TMend %C-indexing
    tStart = tic;
    pp = find( trackingMatrix(:,8) == frame );
    
    lineagesNerrFree{frame+1} = zeros(length(pp),1);
    
    for ii = 1:length(pp)
        parId = trackingMatrix(pp(ii),7);
        count = 0;
        countGT = 0;
        %look backwards until we find a mistake or teh track ends
        while( isKey( treenodeIdMap, parId ) == true )
            nodeIdx = treenodeIdMap( parId );
            %check if element was in ground truth subset and if there was an
            %error
            if( isKey( treenodeIdMapGT, trackingMatrix(nodeIdx,1) ) == true )
                countGT = countGT + 1;
                nodeIdxGT = treenodeIdMapGT( trackingMatrix(nodeIdx,1) );
                if( isKey( treenodeIdMapGT, trackingMatrixGT(nodeIdxGT,7) ) == true && trackingMatrixErrorCode(nodeIdxGT) < 2 )%parent is in the ground truth and segmentation or tracking failed
                    break;
                end
            end
            
            %if we are here it means no error
            count = count + 1;
            parId = trackingMatrix( nodeIdx, 7 );
        end
        
        %look forward until we find a mistake (if tehre is a division we will average results
        chIdx = childrenIdMap( pp(ii), : );
        queue = [];
        if( chIdx(1) > 0 )
            queue = [queue chIdx(1) count countGT];
        end
        if( chIdx(2) > 0 )
            queue = [queue chIdx(2) count countGT];
        end
        
        numPaths = 0;
        lengthPath = zeros(10,1);
        lengthPathGT = lengthPath;
        while( isempty(queue) == false )
            chIdx = queue(1);
            count = queue(2);
            countGT = queue(3);
            queue([1:3]) = [];
            while( chIdx > 0 )
                nodeIdx = chIdx ;
                %check if element was in ground truth subset and if there was an
                %error
                if( isKey( treenodeIdMapGT, trackingMatrix(nodeIdx,1) ) == true )
                    countGT = countGT + 1;
                    nodeIdxGT = treenodeIdMapGT( trackingMatrix(nodeIdx,1) );
                    if( isKey( treenodeIdMapGT, trackingMatrixGT(nodeIdxGT,7) ) == true && trackingMatrixErrorCode(nodeIdxGT) < 2 )%parent is in the ground truth and segmentation or tracking failed
                        break;
                    end
                end
                
                %if we are here it means no error
                count = count + 1;
                chIdx = childrenIdMap( nodeIdx, :);
                if( chIdx(2) > 0 )
                    queue = [queue chIdx(2) count countGT];
                end
                chIdx = chIdx(1);
            end
            
            %we finished one path
            numPaths = numPaths + 1;
            lengthPath( numPaths ) = count;
            lengthPathGT( numPaths ) = countGT;
        end                
        lineagesNerrFree{frame+1}(ii) = mean(lengthPath(1:numPaths));            
        lineagesNerrFreeGT{frame+1}(ii) = mean(lengthPathGT(1:numPaths));            
        
    end
    
    disp(['Processed frame ' num2str(frame) ' in ' num2str(toc(tStart)) 'secs']);
end

toc;