%INPUT:

%imgPatternFilename: optional if we want to generate thumbnails to explore the mistakes and correct

%OUTPUT:
%trackingMatrixGT: Nx10 array with GT from database
%trackingMatrixErrorCode: Nx1 array. 0->false negative (no centroid was
%within the radius of teh GT centroid);1->true positive (only one centroid
%was within radius of GT centroid); 2->oversegmentation (more than one
%centroid was within the radius of the GT centorid)

function [trackingMatrixErrorCode,trackingMatrixGT, maxDistanceSegmentationError, numCells] = segmentationErrorSingleTimePoint(projectName, basenameXMLGMM, imgFilePattern, baseTM)


%main parameters
if(nargin < 1)
    projectName = 'Pairwise Random Sampling GT - Drosophila SiMView'; %project containing the ground truth in CATMAID
    basenameXMLGMM = 'G:\TGMMrunsArchive\GMEMtracking3D_2013_11_11_22_15_4_dataset_12_08_28_drosophila_paperRun\XML_finalResult_lht\GMEMfinalResult_frame';
    imgFilePattern = 'G:/12-08-28/Results/TimeFused.Blending/Dme_E1_His2ARFP.TM?????_timeFused_blending/SPC0_CM0_CM1_CHN00_CHN01.fusedStack_?????.jp2';
    baseTM = 180;
end


%----------------------------------------------------------------------
pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 AND location_t =' num2str(baseTM)];%retireve only for specific time point
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

if( isempty( trackingMatrixGT ) == true )
    trackingMatrixGT = [];
    trackingMatrixErrorCode = [];
    maxDistanceSegmentationError = 0;
    numCells = 0;
    disp 'No GT points for this time point!!!'
    return;
end

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

rmpath( pathAdd );

close(connDB);


%----------------------------------------------------------------------
%%
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode'
%disp('WARNING: testing short par loop!!!!!!');
frame = baseTM;


TGMMfilename = [basenameXMLGMM num2str(frame,'%.4d') '.xml'];
if( exist(TGMMfilename,'file') == 0 )%we do not have more tracking information
    warning 'XML file does nto exist'
    return;
end
obj = readXMLmixtureGaussians(TGMMfilename );

numCells = length(obj);
%load image
imgFilename = recoverFilenameFromPattern(imgFilePattern, frame);
[pathstr, name, ext] = fileparts(imgFilename) ;

if( strcmp(ext,'.jp2') == 1 )
    im = readJPEG2000stack(imgFilename, 8 );
else
    im = readTIFFstack(imgFilename );
end
%get interpolated Z between stacks (manual clicking can be off by +-2 slices
[ZinterpPixels, ~ ] = interpolateZ(im,trackingMatrixGT(:,3:5), scale, 2);
trackingMatrixGT(:, 5 ) = ZinterpPixels * scale(3);


%parse xyz points from automatic tracking
xyz = zeros(length(obj),3);
score = zeros(length(obj),1);
for kk = 1: size(xyz,1)
    xyz(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y
    score(kk) = obj(kk).splitScore;
    if( obj(kk).id ~= kk-1 )
        error 'GMM Index is not sequential!'
    end
end
for kk = 1:3
    xyz(:,kk) = xyz(:,kk) * scale(kk);
end


%%
%--------------------------------------------------------------------
%find out average nuclei radius to set maxDistanceSegmentationError

%read list of supervoxels
svFilename = [basenameXMLGMM num2str(baseTM,'%.4d') '.svb'];
if( exist(svFilename,'file') == 0 )%we do not have more tracking information
    error 'Supervoxels file does not exist!!!!'
else
    [svList, sizeIm] = readListSupervoxelsFromBinaryFile( svFilename );
end

equivRadius = zeros(length(obj),1);
for ii =1:length(obj)
    svIdxVec = obj( ii ).svIdx;
    for bb = 1:length(svIdxVec)
        if( svIdxVec(bb) < 0 )
            continue;
        end
        PixelIdxList = svList{svIdxVec(bb) + 1};%pixelIdxlist
        
        [sx, sy, sz] = ind2sub(sizeIm, PixelIdxList+1);
        sxy = unique([sx sy],'rows');
        equivRadius(ii) = sqrt(length(sxy) / pi);
    end
end

maxDistanceSegmentationError = prctile(equivRadius,75); %max distance to consider that two points are the same between ground truth and automatic reconstruction

%%
%count segmentation errors
trackingMatrixErrorCode = ones(size(trackingMatrixGT,1),1);%by default, points are correct
[idx, dist] = knnsearch(xyz, trackingMatrixGT(:,3:5),'k',2);%dist has the size of GT
trackingMatrixErrorCode( dist(:,1) <= maxDistanceSegmentationError & dist(:,2) <= maxDistanceSegmentationError & (dist(:,2)./ dist(:,1)) < 3.0 ) = 2;%oversegmentation
trackingMatrixErrorCode( dist(:,1) > maxDistanceSegmentationError & (dist(:,2)./ dist(:,1)) < 2.0 ) = 0;%no correspondence; the second rule (ratio > 2.0) was obtained empirically by manually annotating 30 FN results, since just the max_dist threshold is not sufficient

rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode'




