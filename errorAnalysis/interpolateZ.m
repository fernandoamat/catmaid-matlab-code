% % Interpolates z value retireved from CATMAID (since it is saved as an
% % integer) based on image content
% % 
% % INPUT:
% %  xyz:  Nx3 array containg points retireved from CATMAID database (IN NM UNITS!!!)
% %  im:   WxHxD array containing image to interpolate Z value
% % 
% % stackRes: 1x3 array containing the resolution of the dataset (so we can
% go from nm to pixel units) (this information is retireved from the
% database)

% % OUTPUT:
% % Zinterp:    Nx1 array containing interpolated Z value
% % 
% % 
% % WARNING:
% % 
% % 1.-Sometime between CATMAID and imread there is a permuation of x,y
% % coordinates, so make sure they are correct
% % 2.-Matlab uses 1-indexing while catmaid uses zero indexing


function [ZinterpPixels, ZinterpWorld ] = interpolateZ(im,xyz, stackRes, offsetPlane)

w = 5;%plus/minus window to interpolate Z value


%median filter to smooth values in XY (equivalent shoot line over an
%averaged column)
for ii = 1:size(im,3)
   im(:,:,ii) = medfilt2(im(:,:,ii),[5 5]); 
end

%we are using this to correct from manual clicking->things can be off by
%more than one plane
if( isempty(offsetPlane) == true )%default value
    offsetPlane = 1;%we look for maximima at [z-offsetPlane-0.5, z+offsetPlane+0.5]
end

N = size(xyz,1);
%conbvert to pixel units
xyz = xyz ./ repmat(stackRes,[N 1]); 

%permute x,y dimensions
xyz = xyz(:,[2 1 3]);

%add 1 indexing
xyz = xyz + 1;

ZinterpPixels = zeros( N, 1);
qq = [1:0.01:2*w+1];
for ii = 1:N
    p = round(xyz(ii,:));
    
    if( p(3)-w < 1 || p(3)+w>size(im,3) )
        ZinterpPixels(ii) = p(3);
        continue;
    end
    
    patch = squeeze(im( p(1), p(2), p(3) - w: p(3)+w ));
    sp = spline([1:2*w+1],single(patch),qq);
    %figure;plot([0:2*w],patch,'o',[0:0.01:2*w],sp) %uncomment this line to display interpolation
        
    pos = [100 * w - 49 - offsetPlane * 100: 101 * w + 46 + offsetPlane * 100];
    
    [val h] = max(sp(pos));
    ZinterpPixels(ii) = qq(pos(h)) - (w+1) + xyz(ii,3);
end

%undo dimensions transformation
ZinterpPixels = ZinterpPixels - 1;

if( nargout > 1 )
    ZinterpWorld = ZinterpPixels * stackRes(3);
end





