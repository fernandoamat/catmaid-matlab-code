function tesFunction(objCell)

load 'temp/workspace.mat';

%calculate TRA measures
%errC = [NS, FN, FP, ED1, ED2, EA, EC]

%note: ED1 is always zero since it is redundant. When removing a node from
%a graph you already remove the edge to the parent associated with it
errC = zeros( endTM + 1, length( wTRA ) );

numM = zeros(numTM,1);%number of points in groundtruth
numMhat = numM;
numMhatCell = cell(endTM+1,1);
TP = zeros( endTM + 1, 1);% auxiliary variable
for kk = 1:N
    if( trackingMatrix(kk,9) ~= 5 )%not annotated
        continue;
    end
    tt = trackingMatrix(kk,8) + 1;%Matlab indexing
    numM(tt) = numM(tt) + 1;
    numMhatCell{tt} = [ numMhatCell{tt} matchGT2TGMM{kk} ];%number of TGMM detections associated with this element
    
    
    if( distGT2TGMM{kk}(1) > maxDistanceSegmentationError )%FN: GT point was not detected
        errC(tt,2) =  errC(tt,2) + 1;                
    else
        %if we are here it means we have at least one good detection(the
        %nearest neighbor)
        TP( tt ) = TP( tt ) + 1;
        %FP: check if there was oversegmentation
        if( length(distGT2TGMM{kk}) > 1 )
            aa = distGT2TGMM{kk}(2:end) / distGT2TGMM{kk}(1); 
            errC(tt,3) =  errC(tt,3) + sum(aa < 5.0 );
        end
        %EA: check if edge is missing
        if( isKey( treenodeIdMap, trackingMatrix( kk, 7) ) == true )
            %check that parent also had a true positive detection
            parIdx = treenodeIdMap( trackingMatrix( kk, 7) );
            if( distGT2TGMM{ parIdx }(1) <= maxDistanceSegmentationError )
               parTGMM = objCell{tt}( matchGT2TGMM{kk}(1) ).parent + 1;
               if( matchGT2TGMM{ parIdx }(1) ~=  parTGMM)
                  errC(tt,6) = errC(tt,6) + 1; 
               end
            end
        end
    end        
end

%compute numMhat
for kk = 1:length(numMhatCell)
   numMhat(kk) = length(unique(( numMhatCell{kk} ))); 
   
   %NS: check if there was undersegmentation (one TGMM point assigned to two different GT point)
   errC(kk,1) = length(( numMhatCell{kk} )) - numMhat(kk);
end

%compute number of edges in TGMM that need to be removed (ED2)
numUnmatched = zeros(length(numMhatCell),1);
numUnmatchedTest = zeros(length(numMhatCell),1);
for tt = 1:length(numMhatCell)
   numMhatCell{tt} = unique( numMhatCell{tt} ); 
   
   for ii = 1: length( numMhatCell{tt} )
      %find edge in TGMM
      idx = numMhatCell{tt}(ii);
      %find if this object was matched
      if( distTGMM2GT{tt}(idx) > maxDistanceSegmentationError )
          if( matchGT2TGMM{ matchTGMM2GT{tt}(idx) }(1) ~= idx )
              numUnmatchedTest(tt) = numUnmatchedTest(tt) + 1;
             %disp 'Warning: Object was not matched!' 
             numUnmatched(tt) = numUnmatched(tt) + 1;
             continue;
          end
      end
      obj = objCell{tt}( idx );
      idxPar = obj.parent + 1;
      if( idxPar > 0 )
          %objPar = objCell{tt-1}(idxPar);
          %find if this object was matched
          if( distTGMM2GT{tt-1}(idxPar) > maxDistanceSegmentationError )
              numUnmatchedTest(tt) = numUnmatchedTest(tt) + 1;
              if( matchGT2TGMM{ matchTGMM2GT{tt-1}(idxPar) }(1) ~= idxPar )
                  %disp 'Warning: Object parent was not matched!'
                  numUnmatched(tt) = numUnmatched(tt) + 1;
                  continue;
              end
          end
          %find if edge exists in GT          
          if( isKey( treenodeIdMap, trackingMatrix( matchTGMM2GT{tt}(idx), 7) ) == false )
              errC(tt,5) = errC(tt,5) + 1;
          else
              if( treenodeIdMap (trackingMatrix( matchTGMM2GT{tt}(idx), 7) ) ~= matchTGMM2GT{tt-1}(idxPar))
                  errC(tt,5) = errC(tt,5) + 1;
              end
          end
      end                  
   end
end

a=1;
