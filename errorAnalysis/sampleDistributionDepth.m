%calculates convex hull and then returns distance of each point to that
%convex hull

%based on code form Dan Mossing
function depthNuclei = sampleDistributionDepth(xyz,xyzFullEmbryoCoverage)


%calculate embryo surface using convex hull
pp = find( ~isnan(xyzFullEmbryoCoverage(:,1)));
K = convhull(xyzFullEmbryoCoverage(pp,1), xyzFullEmbryoCoverage(pp,2), xyzFullEmbryoCoverage(pp,3));

%calculate distance for each point to the convex hull
addpath C:\Users\Fernando\TrackingNuclei\matlabCode\DanMossingCode

%Note: For Dan this is K = chcell{t,2} and xyzFullEmbryoCoverage = chcell{t,1}
chcell = cell(1,2);
chcell{1,1} = xyzFullEmbryoCoverage;
chcell{1,2} = K; 

xyzt = [xyz zeros(size(xyz,1), 1)];

depthNuclei = getshelldistance(chcell,xyzt);%If I need Dan also returns vector to surface and triangle associated with a point

rmpath C:\Users\Fernando\TrackingNuclei\matlabCode\DanMossingCode



