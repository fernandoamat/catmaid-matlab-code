%compares idfferent annotations in the same time point to see if they look
%randomly distributed
function debugCompareManualGTstats()

baseTM = 130;
scale = [1 1  2.031 / (6.5 / 16)];
pixel2um = (6.5 / 16);
basenameXMLGMG = 'E:\TGMMruns\GMEMtracking3D_2013_10_8_20_24_34_afterAddingHSsplitForDeathExtension_TM0-800\XML_finalResult_lht\GMEMfinalResult_frame';

%parse xyz points from automatic tracking to build convex hull
TGMMfilename = [basenameXMLGMG num2str(baseTM,'%.4d') '.xml'];    
addpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
obj = readXMLmixtureGaussians(TGMMfilename );
rmpath 'C:\Users\Fernando\TrackingNuclei\matlabCode\mixtureGaussiansTracking'
xyzFullCoverage = zeros(length(obj),3);
for kk = 1: size(xyzFullCoverage,1)
    xyzFullCoverage(kk,:) = obj(kk).m([2 1 3]);%CATMAID and TGMM flip X Y        
end

for kk = 1:3
    xyzFullCoverage(:,kk) = xyzFullCoverage(:,kk) * scale(kk);
end


%%
%annotation done with Matlab
qq = load('E:\TGMMruns\GMEMtracking3D_2013_10_8_20_24_34_afterAddingHSsplitForDeathExtension_TM0-800\annotations\TM00130\annotationFile.mat');

xyz = zeros(length(qq.objFinal),3);
for kk = 1:length(qq.objFinal)
    xyz(kk,:) = qq.objFinal(kk).m;
end

%load image
im = readJPEG2000stack(qq.objFinal(1).imFilename,8);

SNRcontrastA = sampleDistributionSNR(xyz,im);

for kk = 1: 3
    xyz(:,kk) = scale(kk) * xyz(:,kk);
end
depthNucleiA = sampleDistributionDepth(xyz(:,[2 1 3]),xyzFullCoverage);

%%
%annotations done by Dan for neuroblast
projectName = '12-08-28 Drosophila TGMM TM1000 Neuroblast';


pathstr = fileparts( mfilename('fullpath') );
pathAdd = [pathstr '/../dbConnection'];
addpath(pathAdd)
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

scale = stackRes /stackRes(1);

if( isempty( strfind(projectName,'12-08-28') ) == false || isempty( strfind(projectName,'Drosophila SiMView') ) == false )
    %stack resolution for project should be [406.3, 406.3, 2031]nm = 1000 * [6.5/16, 6.5/16, 2031] = 1000 * [pixelSize / magnification,..., z-step]
    disp 'CORRECTING SCALING FOR PROJECT SINCE CATMAID DOES NOT HAVE CORRECT INFO'
    scale = [1 1  2.031 / (6.5 / 16)];
end

%retrieve all points with confidence 5 (ground truth)
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 AND location_t >=' num2str(baseTM) ' AND location_t<=' num2str(baseTM+1)];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

SNRcontrastB = sampleDistributionSNR(bsxfun(@mtimes,trackingMatrixGT(:,[4 3 5]),1./scale),im);
depthNucleiB = sampleDistributionDepth(trackingMatrixGT(:,3:5),xyzFullCoverage);

%%
%annotations done by Bill
projectName = '12-08-28 Pairwise Random Sampling GT';
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 AND location_t >=' num2str(baseTM) ' AND location_t<=' num2str(baseTM+1)];
trackingMatrixGT = retrieveLineageFilter( connDB, WHEREclause );

for kk = 1: 3
    trackingMatrixGT(:,kk+2) = scale(kk) * trackingMatrixGT(:,kk + 2) / stackRes(kk);
end

SNRcontrastC = sampleDistributionSNR(bsxfun(@mtimes,trackingMatrixGT(:,[4 3 5]),1./scale),im);
depthNucleiC = sampleDistributionDepth(trackingMatrixGT(:,3:5),xyzFullCoverage);

%%
%display 3 results for SNR
figure;
[p, u] = hist(SNRcontrastA,[0:100]);
plot(u,cumsum(p) / sum(p),'b');
hold on;
[p, u] = hist(SNRcontrastB,[0:100]);
plot(u,cumsum(p) / sum(p),'r');
[p, u] = hist(SNRcontrastC,[0:100]);
plot(u,cumsum(p) / sum(p),'k');
hold off;
xlabel('Contrast');
ylabel('Cumulative percentage of annotations');
title(['TM=' num2str(baseTM) '; dataset = 12-08-28'])
legend('Fernando-Matlab','Dan-NB','Bill-CATMAID','Location','Best');

%%
%display 3 results for depth
figure;
[p, u] = hist(depthNucleiA * pixel2um,[0:100]);
plot(u,cumsum(p) / sum(p),'b');
hold on;
[p, u] = hist(depthNucleiB * pixel2um,[0:100]);
plot(u,cumsum(p) / sum(p),'r');
[p, u] = hist(depthNucleiC * pixel2um,[0:100]);
plot(u,cumsum(p) / sum(p),'k');
hold off;
xlabel('Nuclei depth (\mum)');
ylabel('Cumulative percentage of annotations');
title(['TM=' num2str(baseTM) '; dataset = 12-08-28'])
legend('Fernando-Matlab','Dan-NB','Bill-CATMAID','Location','Best');
%%
close(connDB);