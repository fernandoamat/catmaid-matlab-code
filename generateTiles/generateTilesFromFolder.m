%right now it only does time, so if you have multiple channels you have to
%run it for channel seprately
function generateTilesFromFolder()

numWorkers = 4;
outputCATMAIDprojectFolder = 'R:/CATMAIDdata/Fernando/14-09-26-Mouse-Katie'

%use a cell array of strings for multiple channels


imgFilenamePatternList = {'X:/SiMView1/14-09-26/Mmu_E1_Fucci2_01_20140926_160250.corrected/Results/MultiFused.Corrected/Mmu_E1_Fucci2.TM??????_multiFused_blending/SPM00_TM??????_CM00_CM01_CHN01.fusedStack.corrected.jp2'};

%iniFrame = 101;
%endFrame = 232;
iniFrame = 0;
endFrame = 350;


tileSize = 512; %do not change this. Small tiles create too many small files
thumbnailSize = 64;
maxScales = 1; %for SimView Drosophila set it to 1 (no binned image) because we do not need more and we avoid creating too many small files
imType = '.jpg';

maxNucleusRadius = 60; %in pixels. Only needed for local contrast enhancement
intensityScaling = 1; %0->global linear scaling; 1->CLAHE (local constrast enhancement)

checkIfFolderExists = false; %true->we are running after a crash and we don't want to redo existing folders (=frames)

%========================================================================================

if( round(log2(tileSize)) ~= log2(tileSize) )
    error 'tileSize should be a power of 2'
end

if( exist( outputCATMAIDprojectFolder, 'dir' ) == 0 )
    mkdir( outputCATMAIDprojectFolder );
end

%geenrate subfolder structure
outputCATMAIDprojectFolder = [outputCATMAIDprojectFolder '/stack1' ];
if( exist( outputCATMAIDprojectFolder, 'dir' ) == 0 )
    mkdir( outputCATMAIDprojectFolder );
end

nW = matlabpool('size');
if( nW ~= numWorkers )
    if( nW > 0 )
        matlabpool('close');
    end
    matlabpool(numWorkers);
end


for triview = 1:3 %repeat the proceure for each posible plane: XY, XZ, YZ
    
    
    for ch = 1:length(imgFilenamePatternList)%repeat the procedure for each channel
        
        
        imgFilenamePattern = imgFilenamePatternList{ch};
        switch(triview)
            case 1 %XY view: traditional one
                dstFolderTriview = [outputCATMAIDprojectFolder];
            case 2 %YZ view in Fiji
                dstFolderTriview = [outputCATMAIDprojectFolder '/YZ'];
                if( exist( dstFolderTriview, 'dir' ) == 0 )
                    mkdir(dstFolderTriview);
                end
            case 3 %XZ view in Fiji
                dstFolderTriview = [outputCATMAIDprojectFolder '/XZ'];
                 if( exist( dstFolderTriview, 'dir' ) == 0 )
                    mkdir(dstFolderTriview);
                 end
            otherwise
                error 'Code not ready for this'
        end
        
        dstFolderChannel = [dstFolderTriview '/' num2str(ch-1) ];
        if( exist( dstFolderChannel,'dir' ) == 0 )
            mkdir( dstFolderChannel );
        end
        
        %disp 'WARNING: no parfor debugging!!!!!!!!!!!!!!!!!!!!!!!!!'
        parfor frame = iniFrame: endFrame
            tic;
            %if folder exists, we assume we are rerunning a fter a crash
            dstFolderFrame = [dstFolderChannel '/' num2str(frame-iniFrame) ];
            if( checkIfFolderExists == true && exist( dstFolderFrame, 'dir' ) > 0 )
                disp(['Skipping frame ' num2str(frame) ' because folder exists']);
                continue;
            end
            
            if( exist( dstFolderFrame,'dir' ) == 0 )
                mkdir( dstFolderFrame );
            end
            
            %read stack
            imgFilename = recoverFilenameFromPattern(imgFilenamePattern, frame);
            if( strcmp( imgFilenamePattern(end-3:end), '.jp2' ) == 1 )
                stack = readJPEG2000stack(imgFilename, 8 );
            else
                stack = readTIFFstack(imgFilename);
            end
            
            
            %permute dimensions to generate triviews
            switch(triview)
                case 1 %XY view: traditional one
                    %nothing to do
                case 2 %YZ view in Fiji
                    stack = permute(stack, [1 3 2] );                    
                case 3 %XZ view in Fiji
                    stack = permute(stack, [3 2 1] );
                otherwise
                    error 'Code not ready for this'
            end
            
            
            %global linear normalization. 
            %Later you can use local contrast or other techniques ot
            %enhance view
            stack = single( stack );
            %thr = prctile(stack( stack > 0),[1 100]); %memory peak
            thr = zeros(2,1);
            thr(2) = max( stack(:) );
            thr(1) = min( stack( stack>0 ) );
            stack = ( stack -thr(1) )/ ( thr(2)-thr(1) );
            stack( stack < 0 ) = 0;
            stack( stack > 1 ) = 1;
            
            
            %slice size has to be equal to n*tileSize with n = 2^b
            padDim = [1 1];
            while( padDim(1) * tileSize < size(stack,1) )
                padDim(1) = padDim(1) * 2;
            end
            while( padDim(2) * tileSize < size(stack,2) )
                padDim(2) = padDim(2) * 2;
            end
            padDim = padDim * tileSize - [size(stack,1) size(stack,2)];
            

            
            
            %generate tiles
            for kk = 1:size(stack,3)
                slice = stack(:,:,kk);
                
                %local contrast enhancement (CLAHE)
                if( intensityScaling == 1)
                    blockSize = maxNucleusRadius * 3;%This size should be larger than the size of features to be preserved.
                    numBlocks = round( size(slice) /  blockSize );%tile size will be tileSize x tileSize
                    numBlocks = max(numBlocks,2);
                    %we assume slice has been normalized to single [0,1]
                    slice = adapthisteq(slice,'numtiles',numBlocks, 'clipLimit',0.01,'Nbins',256,'Distribution','uniform'); %CLAHE works better with single                  
                end
                %pad array to a multiple of the tile size
                slice = padarray(slice, padDim, 0, 'post');
                
                %generate subfolder for slice
                dstFolderSlice = [dstFolderFrame '/' num2str(kk-1) ];
                if( exist( dstFolderSlice,'dir' ) == 0 )
                    mkdir(dstFolderSlice);
                end
                
                %generate tiles
                generateTile(slice,dstFolderSlice, tileSize, 0, maxScales, imType);
                
                %save thumbnail
                thumbnail = imresize(slice, [thumbnailSize thumbnailSize], 'bicubic','Antialiasing',true);
                imwrite(uint8(thumbnail * 255 ), [dstFolderSlice '/small' imType]);
            end
            diffTime = toc;
            disp(['Finished frame ' num2str(frame) ' in ' num2str(diffTime) 'secs']);
        end
        
    end
    
end




