treenodeIdMap = containers.Map(trackingMatrix(:,1),[1:size(trackingMatrix,1)]);
%find for each skeleton the minimum time point that was curated
skeletonIdArray = unique(trackingMatrix(:,10));
skeletonIdT = zeros( length(skeletonIdArray) , 1);
for kk = 1: length(skeletonIdArray)
    pp = find(trackingMatrix(:,10) == skeletonIdArray(kk),1,'first');
    skeletonIdT(kk) = trackingMatrix(pp,8);    
end
skeletonIdMap = containers.Map(skeletonIdArray, skeletonIdT);



pos = zeros(10000,1);
posN = 0;
for kk = 1: size(trackingMatrix,1)
    par = trackingMatrix(kk,7);
    skel = trackingMatrix(kk,10);
    if( par < 0 )
        continue;
    end
    
    if( trackingMatrix(kk,8) == skeletonIdMap( skel ) )
        continue;%first element of this lineage that was annotated
    end
    
    if( isKey(treenodeIdMap,par) == false )
        %posN = posN + 1;
        %pos(posN) = kk;
        continue;
    else
        parId = treenodeIdMap(par);
        if( trackingMatrix(parId,8) - trackingMatrix(kk,8) ~= -1 )
            posN = posN + 1;
            pos(posN) = kk;
        end
    end
end

pos = pos(1:posN);