projectName = '13-03-12-Platenaries-CLAHE TGMM noSL'; %project containing the ground truth in CATMAID

%----------------------------------------------------------------------
%obtain annotated ground truth from CATMAID database for a specific project
%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();


%connect to database
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
WHEREclause = ['project_id = ' '''' num2str(projectId) ''' AND location_t <= 600'];

trackingMatrix = retrieveLineageFilter( connDB, WHEREclause );

close(connDB);


