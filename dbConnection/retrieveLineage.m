function [trackingMatrix, tags] = retrieveLineage( connDB, projectId )

%{
numWorkers = 4;%we do not gain anything with more wrokers

if( matlabpool('size') ~= numWorkers  && numWorkers > 1)
    if( matlabpool('size') > 0 )
        matlabpool('close');
    end
    matlabpool(numWorkers);
end
%}

curs = exec(connDB,['SELECT id, location, parent_id, radius, confidence, location_t, location_c, skeleton_id  FROM treenode WHERE project_id = ' '''' num2str(projectId) '''' ' ORDER BY location_t']);
%curs = exec(connDB,['SELECT id, location, parent_id, radius, confidence, location_t, location_c, skeleton_id  FROM treenode WHERE project_id = ' '''' num2str(projectId) '''' ' AND confidence >= 3' ' ORDER BY location_t']);
setdbprefs('DataReturnFormat','structure');%location_t cannot be retrieved as numeric
curs = fetch( curs );

%we follow the SWC convention (more or less, since we need time )
%id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id

trackingMatrix = zeros( length(curs.Data.id ), 10 );

trackingMatrix(:, 1) = curs.Data.id;
trackingMatrix(:, 6) = curs.Data.radius;
trackingMatrix(:, 7) = curs.Data.parent_id;
trackingMatrix( isnan(trackingMatrix(:, 7)) , 7) = -1;
trackingMatrix(:, 8) = curs.Data.location_t;
trackingMatrix(:, 9) = curs.Data.confidence;
trackingMatrix(:, 10) = curs.Data.skeleton_id;

qq = curs.Data.location;
aux = zeros(size(trackingMatrix,1),3);
disp 'Start parsing double3d'
for kk = 1:size(trackingMatrix, 1) %for some reason the code crashes with parfor    
    %trackingMatrix(kk, 3:5) = sscanf(char(qq{kk}),'(%f,%f,%f)')';%I cannot do this inside a parfor
    aux(kk,:) = sscanf(char(qq{kk}),'(%f,%f,%f)')';
end
trackingMatrix(:, 3:5 ) = aux;

setdbprefs('DataReturnFormat','cellarray');%restore default value

%find tags
tags = [];
if( nargout > 1 )
    tags = cell( size(trackingMatrix,1), 1);
    
    %build map for tags
    curs = exec(connDB,['SELECT id,name FROM class_instance WHERE  project_id = ' '''' num2str(projectId) '''']);
    curs = fetch(curs);
    labels = containers.Map(curs.Data(:,1), curs.Data(:,2));
    
    %build map for treenode_id to index in trackingMatrix
    treenodeMap = containers.Map(trackingMatrix(:,1),[1:size(trackingMatrix,1)]);
    
    %retrieve all tags from project to assign to each node (some nodes
    %might have more than one tag)
    setdbprefs('DataReturnFormat','numeric');%location_t cannot be retrieved as numeric
    curs = exec(connDB,['SELECT treenode_id,class_instance_id FROM treenode_class_instance WHERE  project_id = ' '''' num2str(projectId) '''']);
    curs = fetch(curs);
    
    sizeT = 1;
    for kk = 1: size(curs.Data,1)
        
       if( isKey( treenodeMap, curs.Data(kk,1) ) == false )
           continue;
       end
       idx = treenodeMap(curs.Data(kk,1));
       
       if( isempty( tags{idx,sizeT} ) == false )
           sizeT = sizeT + 1;
           tags{idx,sizeT} = [];
       end
       pos = -1;
       for ii = 1:sizeT
          if(  isempty( tags{idx,ii} ) == true )
              pos = ii;
              break;
          end
       end
       
       %add tag
       tags{idx,pos} = labels(curs.Data(kk,2));
    end
    setdbprefs('DataReturnFormat','cellarray');%restore default value
end

close(curs);

