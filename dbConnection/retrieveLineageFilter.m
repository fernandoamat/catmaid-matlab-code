%retireve lineage filter with by specific criteria
%example WHEREclause = ['project_id = ' '''' num2str(projectId) '''' ' AND confidence=5 ']
function trackingMatrix = retrieveLineageFilter( connDB, WHEREclause )


curs = exec(connDB,['SELECT id, location, parent_id, radius, confidence, location_t, location_c, skeleton_id  FROM treenode WHERE ' WHEREclause ' ORDER BY location_t']);
setdbprefs('DataReturnFormat','structure');%location_t cannot be retrieved as numeric
curs = fetch( curs );

%we follow the SWC convention (more or less, since we need time )
%id, type, x, y, z, radius, parent_id, time, confidence


if( isstruct( curs.Data ) ) %otherwise it means the query did not return any result
    trackingMatrix = zeros( length(curs.Data.id ), 10 );
    
    trackingMatrix(:, 1) = curs.Data.id;
    trackingMatrix(:, 6) = curs.Data.radius;
    trackingMatrix(:, 7) = curs.Data.parent_id;
    trackingMatrix( isnan(trackingMatrix(:, 7)) , 7) = -1;
    trackingMatrix(:, 8) = curs.Data.location_t;
    trackingMatrix(:, 9) = curs.Data.confidence;
    trackingMatrix(:, 10) = curs.Data.skeleton_id;
    
    
    %disp 'Start parsing double3d'
    %tic;
    
    %{
qq = curs.Data.location;
aux = zeros(size(trackingMatrix,1),3);
for kk = 1:size(trackingMatrix, 1) %for some reason the code crashes with parfor
    %trackingMatrix(kk, 3:5) = sscanf(char(qq{kk}),'(%f,%f,%f)')';%I cannot do this inside a parfor
    aux(kk,:) = sscanf(char(qq{kk}),'(%f,%f,%f)')';
end
trackingMatrix(:, 3:5 ) = aux;
    %}
    
    %it is equally fast (just more compact)
    trackingMatrix(:, 3:5 ) = cell2mat( cellfun(@(x) ( (sscanf(char(x),'(%f,%f,%f)') )' ), curs.Data.location, 'UniformOutput',0) );
    %toc;
    
else
    trackingMatrix = [];
end
setdbprefs('DataReturnFormat','cellarray');%restore default value
%Database connections, cursors, and resultset objects remain open until you close them using the close function. 
%Always close a cursor, connection, or resultset when you finish using it. Close a cursor before closing the connection used for that cursor.
close(curs);
